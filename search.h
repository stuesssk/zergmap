#ifndef SEARCH_H
#define SEARCH_H

#include <stdio.h>
#include <stdlib.h>

#include "map.h"

int
vertexIndivEdgeCount(graph *zergGraph, int zergFrom);

int
search(graph *zergPos, int zergFrom);

void
cleanNodes(graph * zergGraph);

bool
breadFirstSearch(graph * zergPos, struct node *source, int zergFrom);

#endif
