#ifndef MAP_H
#define MAP_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "struct.h"

struct edge {
	double dist;
	struct node *to;
	struct edge *next;
};

struct node {
    int zergId;
    double latitude;
    double longitude;
    double altitude;
    bool visited;
    bool connected;
    bool tooClose;
    int numEdges;
	struct edge *edges;
	struct node *next;
};

typedef struct adjllistGraph {
	struct node *vertices;
}graph;

enum
{INF = 999999};

double 
distance(double lat1, double long1, double altitude1, double lat2,
         double long2, double altitude2);

graph*
createGraph(void);

void
destroyGraph(graph *zergGraph);

bool
graphMakeVertex(graph *zergGraph, int zergId, double latitude,
                double longitude, double altitude);

bool
graphMakeEdge(graph *zergGraph, int zergFrom, int zergTo, double dist);

size_t
nodeCount(graph *zergGraph);

void
edgeCount(graph *zergGraph);

int
vertexEdgeCount(graph *zergGraph, bool returnLowest);

bool
deleteZerg(graph *zergGraph, int markedZerg);

#endif
