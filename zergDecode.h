#ifndef ZERGDECODE_H
#define ZERGDECODE_H

#include "map.h"

// Size of pcap File Header
enum
{PCAP_FILE_HEADER = 24 };

// Size of pcap File Header
enum
{PCAP_PACKET_HEADER = 16 };

// Size of Ethernet Frame
enum
{ETHERNET_FRAME = 14 };

// Size of IPv4 Header
enum
{IPV4_HEADER = 20 };

// Size of IPv4 Header
enum
{IPV6_HEADER = 40 };

// Size of UDP Header
enum
{UDP_HEADER = 8 };

graph*
decodeFunc(char *argument, size_t percentHealth, graph *zergPos,
           int **zergHealth);

#endif
