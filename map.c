#include <math.h>
#include <stdlib.h>

#include "map.h"

double
distance(double latitude1, double longitude1, double altitude1,
         double latitude2, double longitude2, double altitude2)
{
    double pi = acos(-1);

    // Mean radius of earth 6,371,000m assuming the planet we are using is
    // perfect sphere
    double earthRadius = 6371000;
    double dx = 0;
    double dy = 0;
    double dz = 0;
    double longDiff = longitude1 - longitude2;

    // C likes to have a Rad time with its trig functions
    // Must convert from degrees to radians.
    longDiff *= pi / 180;
    latitude1 *= pi / 180;
    latitude2 *= pi / 180;

    // Planer Distance caclulation adapted from
    //https://rosettacode.org/wiki/Haversine_formula#C
    dz = sin(latitude1) - sin(latitude2);
    dx = cos(longDiff) * cos(latitude1) - cos(latitude2);
    dy = sin(longDiff) * cos(latitude1);
    // Calculate Planer Distance
    double planer =
        asin(sqrt(dx * dx + dy * dy + dz * dz) / 2) * 2 * earthRadius;
    // Calculates true distance using Pathagorian Therom using Alt. and 
    // planer distance.
    double distance = sqrt(pow(planer, 2) + pow((altitude1 - altitude2), 2));

    return distance;
}


graph * createGraph(void)
{
    // Creating the Graph
    graph *zergGraph = malloc(sizeof(*zergGraph));
    if (!zergGraph)
    {
        return NULL;
    }
    zergGraph->vertices = NULL;
    return zergGraph;
}

void
destroyGraph(graph * zergGraph)
{
    // Destroy Function based on sample code found in "Mastering Algorithms in 
    // C" by Kyle Louden and 
    // http://www.geeksforgeeks.org/graph-and-its-representations/
    if (!zergGraph)
    {
        return;
    }
    // Burn it to the ground. Leave no malloc left untouched
    struct node *headNode = zergGraph->vertices;
    while (headNode)
    {
        struct edge *tempList = headNode->edges;
        // Free the adjacency list
        while (tempList)
        {
            struct edge *tmp = tempList->next;

            free(tempList);
            tempList = tmp;
        }
        struct node *tmp = headNode->next;
        // Adj list is freed, free the node
        free(headNode);
        headNode = tmp;
    }
    free(zergGraph);
}

bool
graphMakeVertex(graph * zergGraph, int zergId, double latitude,
                double longitude, double altitude)
{
    // Adding vertex Function based on sample code found in "Mastering 
    // Algorithms in C" by Kyle Louden and 
    // http://www.geeksforgeeks.org/graph-and-its-representations/
    if (!zergGraph)
    {
        return false;
    }
    struct node *newNode = malloc(sizeof(*newNode));
    if (!newNode)
    {
        return false;
    }
    // Initilize the node
    newNode->zergId = zergId;
    newNode->latitude = latitude;
    newNode->longitude = longitude;
    newNode->altitude = altitude;
    newNode->edges = NULL;
    newNode->visited = false;
    newNode->connected = false;
    newNode->tooClose = false;
    newNode->numEdges = 0;
    newNode->next = zergGraph->vertices;
    // add the node to the graph
    zergGraph->vertices = newNode;

    struct node *present = zergGraph->vertices;

    // Node has been added, connect the edges
    while (present)
    {
        if (present->zergId == newNode->zergId)
        {
            present = present->next;
            continue;
        }
        // Add the forward and reverse edge
        // Let's find the distance (using as weight between nodes)
        double dist = distance(newNode->latitude, newNode->longitude,
                               newNode->altitude, present->latitude,
                               present->longitude, present->altitude);

        // checking if distance is below minimum threshold for zerg
        // to effectivly communucate. This will only flag the last zerg
        // of the pair to be flaged.
        if (dist < 1.143)
        {
            newNode->tooClose = true;
        }
        // Make the edges, from source to destination
        // and from destination to source. Since undirercted graph
        graphMakeEdge(zergGraph, newNode->zergId, present->zergId, dist);
        graphMakeEdge(zergGraph, present->zergId, newNode->zergId, dist);
        present = present->next;
    }
    return true;
}

bool
graphMakeEdge(graph * zergGraph, int zergFrom, int zergTo, double dist)
{
    // Adding Edge Function based on sample code found in "Mastering 
    // Algorithms in C" by Kyle Louden and 
    // http://www.geeksforgeeks.org/graph-and-its-representations/

    if (!zergGraph)
    {
        return false;
    }
    struct node *nodeSource = NULL;
    struct node *nodeDest = NULL;

    // These are the nodes you are looking for
    // Stepping through the graph looking for the to/from nodes
    struct node *temp = zergGraph->vertices;

    // !something || !else is equivilent to !(something && else)
    // DSA insured we learn that
    while (temp && !(nodeSource && nodeDest))
    {
        if (temp->zergId == zergFrom)
        {
            nodeSource = temp;
        }
        if (temp->zergId == zergTo)
        {
            nodeDest = temp;
        }
        temp = temp->next;
    }
    // Couldn't find the droids
    if (!(nodeSource && nodeDest))
    {
        return false;
    }

    // Don't create link if distance is greater than or equal to 15m
    if (dist >= 15)
    {
        return false;
    }
    // Check if an edge already exists from the source to the destination
    struct edge *tmp = nodeSource->edges;

    while (tmp != NULL)
    {
        if (tmp->to == nodeDest)
        {
            tmp->dist = dist;
            return true;
        }
        tmp = tmp->next;
    }

    // Insert new edge to the adjacency list of the from node
    struct edge *edgeCreate = malloc(sizeof(*edgeCreate));

    if (!edgeCreate)
    {
        return false;
    }
    edgeCreate->dist = dist;
    edgeCreate->to = nodeDest;
    edgeCreate->next = nodeSource->edges;
    nodeSource->edges = edgeCreate;
    return true;
}


size_t
nodeCount(graph * zergGraph)
{
    // Code used to step trhough adjacency list derived for code found at:
    // www.sanfoundry.com/c-program-adjacency-list/
    if (!zergGraph)
    {
        return 0;
    }
    // Smiple function, just counts the number of nodes I have
    size_t count = 0;
    struct node *present = zergGraph->vertices;
    while (present != NULL)
    {
        ++count;
        present = present->next;
    }
    return count;
}

void
edgeCount(graph * zergGraph)
{
    // Code used to step through adjacency list derived for code found at:
    // www.sanfoundry.com/c-program-adjacency-list/
    if (!zergGraph)
    {
        return;
    }
    // Stores how many nodes are adjacent to each node in the graph
    size_t count = 0;
    struct node *present = zergGraph->vertices;
    while (present != NULL)
    {
        count = 0;
        struct edge *check = present->edges;
        while (check)
        {
            ++count;
            check = check->next;
        }
        // storing the number of adjacencies
        present->numEdges = count;
        present = present->next;
    }
}

int
vertexEdgeCount(graph * zergGraph, bool returnLowest)
{
    // Code used to step trhough adjacency list derived for code found at:
    // www.sanfoundry.com/c-program-adjacency-list/
    int count = 0;

    // set intial lowest to arbitraily high number to ensure we can
    // still test the first node we encounter.
    int lowestCount = INF;
    struct node *present = zergGraph->vertices;
    int lowestZerg = present->zergId;

    // Iterate through each vertex
    while (present != NULL)
    {
        struct edge *check = present->edges;

        count = 0;
        // Iterate through each edge in the adjacency list
        // and increment count. Reset in next loop through
        while (check)
        {
            ++count;
            check = check->next;
        }
        // Low hanging fruit for removal is 1 or 0 adjacencies
        if (count < 2)
        {
            return present->zergId;
        }
        // Checking if we found the node with the lowest number of adjacencies
        // and storing it if we have
        if (count < lowestCount)
        {
            lowestZerg = present->zergId;
            lowestCount = count;
        }
        present = present->next;
    }
    // Setup to return node with least number of adjacencies if returnLowest
    // is passed as true
    if (returnLowest)
    {
        return lowestZerg;
    }
    return 0;
}

bool
deleteZerg(graph * zergGraph, int markedZerg)
{
    // Deleting Node Function based on sample code found in "Mastering 
    // Algorithms in C" by Kyle Louden and 
    // www.geeksforgeeks.org/delete-a-given-node-in-linked-list-under-given
    // -constraints
    bool nodeRemoved = false;

    if (!zergGraph)
    {
        return false;
    }

    struct node *position = zergGraph->vertices;
    struct node *lastNode = position;
    struct node *target = NULL;

    if (position == NULL)
    {
        return nodeRemoved;
    }

    else if (position->zergId == markedZerg)
    {
        // Testing if droid we are looking for is the head of the graph
        zergGraph->vertices = position->next;
        struct edge *temp = position->edges;
        // Sacrificing the edges to our valgrind overlords
        while (temp)
        {
            struct edge *tmp = temp->next;

            free(temp);
            temp = tmp;
        }
        // After the edges, our valgrind overlords require a sacrifice of
        // the node.
        target = position;
        nodeRemoved = true;
        free(position);
        position = zergGraph->vertices;

    }

    // Searching through entire graph for edges to remove and any nodes
    // in need of removal and the node we want removed (if it is not the
    // head node).
    while (position != NULL)
    {
        // This is the node you're looking for. 
        // Burn it to the ground. Burn it with fire.
        if (position->zergId == markedZerg)
        {
            lastNode->next = position->next;
            // Free the edges
            struct edge *edgeRemove = position->edges;
            while (edgeRemove)
            {
                struct edge *tmp = edgeRemove->next;
                free(edgeRemove);
                edgeRemove = tmp;
            }
            // free the node
            target = position;
            free(position);
            nodeRemoved = true;
            position = lastNode->next;
            continue;
        }
        // Need to remove the node we are looking for, when it is in the 
        // adjacency list of other verticies
        struct edge *edgeNow = position->edges;
        struct edge *edgeLast = edgeNow;
        if (edgeNow == NULL)
        {
            // Move along, this is not the edge we are looking for
            lastNode = position;
            position = position->next;
            continue;
        }
        // There is an entry to our target node, in this nodes adjacency list
        else if ((target && edgeNow->to == target) ||
                 (edgeNow->to->zergId == markedZerg))
        {
            // Remove edge in other nodes adjacency list
            position->edges = edgeNow->next;
            free(edgeNow);
            lastNode = position;
            position = position->next;
            continue;
        }
        // Ensure adjacency list is readjusted after removal of an edge
        while (edgeNow != NULL)
        {
            if ((target && edgeNow->to == target) ||
                (edgeNow->to->zergId == markedZerg))
            {
                // Moving edges up the adjacency list to fill in the gap from
                // removing an edge
                edgeLast->next = edgeNow->next;
                free(edgeNow);
                break;
            }
            edgeLast = edgeNow;
            edgeNow = edgeNow->next;
        }
        // Continue checking for the node your looking for (Needs more Star Wars)
        // or any other edges that connect to node to other nodes.
        lastNode = position;
        position = position->next;
    }
    return nodeRemoved;
}
