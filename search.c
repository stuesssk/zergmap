#include "search.h"

int
vertexIndivEdgeCount(graph * zergGraph, int zergFrom)
{
    // Code used to step trhough adjacency list derived for code found at:
    // www.sanfoundry.com/c-program-adjacency-list/
    int count = 0;

    struct node *present = zergGraph->vertices;

    // Iterate through each vertex
    while (present != NULL)
    {
        if (present->zergId == zergFrom)
        {
            present->connected = true;
            struct edge *check = present->edges;

            count = 0;
            // Iterate through each edge in the adjacency list
            // and increment count.
            while (check)
            {
                ++count;
                check->to->connected = true;
                check = check->next;
            }
            return count + 1;
        }
    }
    return 0;
}




int
search(graph * zergPos, int zergFrom)
{
    // Idea for Breadth-first search in conjkunction with depth-first to test
    // disjoint paths came from S. Cuillo.
    struct node *temp = zergPos->vertices;
    struct node *source = zergPos->vertices;

    // Conduct a breadth-first search on all the nodes
    while (temp)
    {
        // Testing all nodes against zergFrom, it can be excluded
        if (temp->zergId == zergFrom)
        {
            temp = temp->next;
        }
        else
        {
            temp->visited = true;
            source = temp;
            //conduct a depth-first search back ato zergfrom
            bool path = breadFirstSearch(zergPos, source, zergFrom);

            if (!path)
            {
                // If there is not any path let main know to delete
                // a node
                return 1;
            }
            // If two disjoint paths then the node is good
            path = breadFirstSearch(zergPos, source, zergFrom);
            if (!path)
            {
                // If there is not a secound path let main know to delete
                // a node
                return 1;
            }
            cleanNodes(zergPos);
        }
        if (temp)
        {
            temp = temp->next;
        }
    }
    // If all nodes have two disjoint paths to zergFrom, then the graph is
    // fully connected
    return 0;
}


void
cleanNodes(graph * zergGraph)
{
    // Set visited to false on all nodes
    if (zergGraph == NULL)
    {
        return;
    }
    struct node *temp = zergGraph->vertices;

    while (temp)
    {
        temp->visited = false;
        temp = temp->next;
    }
}

bool
breadFirstSearch(graph * zergPos, struct node *source, int zergFrom)
{
    // Looking for two disjoint paths back to starting node. Path does not
    // matter. Breadth-First is more ideal than Dijkstra's Algorithm.  INF
    // Weight on all internal paths ensure priority is given to any outside
    // edge.
    bool testReturn = false;
    struct node *temp = zergPos->vertices;

    source->visited = true;
    struct edge *adj = source->edges;

    // If svisited check next on list
    if (adj->to->visited)
    {
        adj = adj->next;
    }
    // Ensuring true gets passed trough the recusion if path is found
    if (adj->to->zergId == zergFrom)
    {
        return true;
    }
    //executing depth first search
    while (adj && adj->to->zergId != zergFrom)
    {
        while (temp)
        {
            // Use outside edges first. All interior edges will have
            // weight of INF.
            if (temp->zergId == adj->to->zergId && !temp->visited &&
                (int) (adj->dist) != INF)
            {
                source = temp;
                testReturn = breadFirstSearch(zergPos, source, zergFrom);
                if (testReturn)
                {
                    return true;
                }
            }
            // Priority will be given to outside edges. If there are no more
            // interior edges can be used.
            else if (temp->zergId == adj->to->zergId && !temp->visited &&
                     (int) (adj->dist) == INF)
            {
                source = temp;
                testReturn = breadFirstSearch(zergPos, source, zergFrom);
                if (testReturn)
                {
                    return true;
                }
            }
            temp = temp->next;
        }
        adj = adj->next;
    }
    return false;
}
