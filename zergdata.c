// NEED THIS #DEFINE to use M_PI constant from math.h
#define _GNU_SOURCE

#include <arpa/inet.h>

#include <math.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "zergdata.h"
#include "struct.h"


uint64_t
ntonll(uint64_t value)
{
    // uint64_t conversion from little endian to big endian, taken from:
    // stackoverflow.com/questions/3022552/is-there-any-standard-htonl-like-
    // function-for-64-bits-integers-in-c
    int num = 42;

    // Testing Endianness 
    if (*(char *) &num == 42)
    {
        // swap if Little Endian
        return ((uint64_t) ntohl(value & 0xFFFFFFFF) << 32LL |
                ntohl(value >> 32));
    }
    else
    {
        // Return if Big Endian already
        return value;
    }
}


zergData *
zergStatus(FILE * fp, int length, zergData * newStatus)
{
    struct statuspayload zergStatus;

    int read = fread(&zergStatus, ZERG_HEADER, 1, fp);

    if (read < 1)
    {
        perror("Could not read Zerg Status Data");
        return NULL;
    }

    unsigned int maxHp = (ntohl(zergStatus.maxHp) >> 8 & 0xFFF);
    int hp = (ntohl(zergStatus.hp) >> 8 & 0xFFF);

    // Zerg header of 12 bytes and status payload of fixed 12 bytes
    // not counting zerg name.  length - 23 ensures null terminated
    char *name = calloc((length - 11), sizeof(char));

    fread(name, (length - ZERG_HEADER), 1, fp);
    if (read < 1)
    {
        perror("Could not read Zerg Status Data");
        return NULL;
    }
    // Debug printstament to ensure proper data reading in from 
    // Zerg status packet
    //printf("HP        : %d/%d\n", hp, maxHp);

    newStatus->hitPoints = hp;
    newStatus->maxHp = maxHp;

    free(name);
    return newStatus;
}


void
zergCommand(FILE * fp)
{
    // Reading in any command packets. Don't need any of the information
    uint16_t command;
    int read = fread(&command, 2, 1, fp);

    if (read < 1)
    {
        printf("Could not read Command Data");
    }
    uint16_t param1;
    uint32_t param2;

    if (command % 2)
    {

        fread(&param1, 2, 1, fp);
        fread(&param2, 4, 1, fp);
    }

}


position *
zergGPS(FILE * fp, int payloadSize, position * newGps)
{
    // Union used to convert binary 32 into a decimal number
    // Union conversion taken from:
    // stackoverflow.com/questions/28257524/convert-iee-754-floating-point-
    // back-to-decimal-c-language
    union
    {
        uint32_t hex;
        float num;
    } tmp;

    struct gpspayload gpsData;

    int read = fread(&gpsData, (payloadSize), 1, fp);

    if (read < 1)
    {
        return NULL;
    }

    union
    {
        uint64_t hex;
        double num;
    } temp;

    temp.hex = ntonll(gpsData.latitude);
    double latitude = temp.num;

    // Reject if recieved Latitude is greater than ±90deg
    if (abs(latitude) > 90)
    {
        printf("Recieved Lattitude outside of acceptable bounds‽");
        return NULL;
    }

    temp.hex = ntonll(gpsData.longitude);
    double longitude = temp.num;

    // Reject if recieved Longitude is greater than ±180deg
    if (abs(longitude) > 180)
    {
        printf("Recieved Longitude outside of acceptable bounds‽");
        return NULL;
    }

    // Conversion from fathoms to meters meters = 1.8288 fathoms
    tmp.hex = ntohl(gpsData.altitude);
    double altitude = tmp.num * 1.8288;

    newGps->latitude = latitude;
    newGps->longitude = longitude;
    newGps->altitude = altitude;

    return newGps;
}


void
zergMessage(FILE * fp, int payloadSize)
{
    // Reading i the message. Don't need to store or display it

    // Adding 1 to payloadSize ensures string is NULL terminated
    char *payload = calloc((payloadSize + 1), sizeof(char));

    fread(payload, (payloadSize), 1, fp);
    free(payload);
}
