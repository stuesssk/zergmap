#include <arpa/inet.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <sysexits.h>
#include <sys/stat.h>

#include "struct.h"
#include "zergdata.h"
#include "zergDecode.h"


graph *
decodeFunc(char *argument, size_t percentHealth, graph * zergPos,
           int **zergHealth)
{
    position *gps = (position *) malloc(sizeof(position));
    zergData *zergInfo = (zergData *) malloc(sizeof(zergData));

    //zergStatusTree* zergHealth = NULL;

    struct capHeader fileHeader = { 0 };
    struct packetHeader pHead = { 0 };
    struct ethFrame ethernet = { 0 };
    union
    {
        struct ipv4Header ipv4;
        struct ipv6Header ipv6;
    } ipheader;

    //struct ipv4Header ipv4 = { 0 };
    struct udpHeader udp = { 0 };
    struct zergHeader zerg = { 0 };

    // Calculating file size in bytes taken from:
    // stackoverflow.com/question/238603/how-can-i-get-a-file-size-in-c
    struct stat fileStat;

    stat(argument, &fileStat);
    int size = fileStat.st_size;

    // Ensuring file is not empty (size == 0)
    if (size == 0)
    {
        fprintf(stderr, "Input File is of size 0\n");
        return NULL;
    }

    FILE *fp = fopen(argument, "rb");

    if (!fp)
    {
        fprintf(stderr, "Unable to open file‽\n");
        return NULL;
    }


    // PCAP file has fixed length of 94 bytes before a zerg payload
    // any PCAP with less than 94 bytes is an invalid packet
    if (size < 94)
    {
        fprintf(stderr,
                "PCAP file is smaller than minimum" " PCAP size of 98 Bytes.");
        fclose(fp);
        return NULL;
    }
    // subtracting 24 bytes (size of PCAP File header) to have running
    // total of bytes that are physically left in file
    size -= sizeof(fileHeader);

    // Read in pcap File Header (only once per file)
    int read = fread(&fileHeader, PCAP_FILE_HEADER, 1, fp);

    if (read < 1)
    {
        fprintf(stderr, "Could not read pcap File Header");
        fclose(fp);
        return NULL;
    }

    // Ensure file is actually a PCAP file
    if (fileHeader.fileID != 0xa1b2c3d4)
    {
        fprintf(stderr, "Invalid PCAP file\n");
        fclose(fp);
        return 0;
    }

    // Variable to track total header length, used for determining
    // if there is any buffer
    int headerSize = 0;

    while (true)
    {
        // Read in pcap Packet Header
        read = fread(&pHead, PCAP_PACKET_HEADER, 1, fp);
        if (read < 1)
        {
            fprintf(stderr, "Could not read pcap Packet Header");
            fclose(fp);
            return NULL;
        }
        // Bytes remaining in file
        size -= sizeof(pHead);

        // Read in Ethernet frame
        read = fread(&ethernet, ETHERNET_FRAME, 1, fp);
        if (read < 1)
        {
            fprintf(stderr, "Could not read Ethernet Frame");
            fclose(fp);
            return NULL;
        }
        headerSize += ETHERNET_FRAME;
        // Bytes remaining in file
        size -= sizeof(ethernet);

        // Checking ethernet frame's ethernet type to determine if using
        // IPv4 of IPv6 header and reading in the apporpriate headers type.
        if (ntohs(ethernet.ethType) == 0x0800)
        {
            headerSize += IPV4_HEADER;
            //printf("IP Version: IPv4\n");
            read = fread(&ipheader.ipv4, IPV4_HEADER, 1, fp);
            if (read < 1)
            {
                fprintf(stderr, "Could not read Ipv4 Header");
                fclose(fp);
                return NULL;
            }
            // checking for length mismatch in ipv4 length to bytes
            // total bytes reminaing in file
            if (size < ntohs(ipheader.ipv4.length))
            {
                fprintf(stderr,
                        "Packet length mismatch."
                        " Not enough bytes left in file\n");
                fclose(fp);
                return NULL;
            }
            // Bytes remaining in file
            size -= sizeof(ipheader.ipv4);
        }
        else if (ntohs(ethernet.ethType) == 0x86DD)
        {
            headerSize += IPV6_HEADER;
            //printf("IP Version: IPv6\n");
            read = fread(&ipheader.ipv6, 40, 1, fp);
            if (read < 1)
            {
                fprintf(stderr, "Could not read Ipv6 Header");
                fclose(fp);
                return 0;
            }
            // checking for length mismatch in ipv4 length to bytes
            // total bytes reminaing in file
            if (size < ntohs(ipheader.ipv6.payloadLen))
            {
                fprintf(stderr,
                        "Packet length mismatch."
                        " Not enough bytes left in file\n");
                fclose(fp);
                return NULL;
            }
            // Bytes remaining in file
            size -= sizeof(ipheader.ipv6);
        }
        else
        {
            fprintf(stderr, "Invalid Ethernet Type");
            return NULL;
        }


        // Read in UDP Header
        read = fread(&udp, UDP_HEADER, 1, fp);
        if (read < 1)
        {
            fprintf(stderr, "Could not read UDP Header");
            fclose(fp);
            return NULL;
        }
        headerSize += UDP_HEADER;
        // checking for length mismatch in udp length to bytes total bytes
        // reminaing in file
        if (size < ntohs(udp.length))
        {
            fprintf(stderr,
                    "Packet length mismatch."
                    " Not enough bytes left in file\n");
            fclose(fp);
            return NULL;
        }
        // Ensure proper UDP port
        if (ntohs(udp.dPort) != 0x0EA7)
        {
            printf("Improper UDP Destination port");
            // Read in the rest of the packet
            int amount = ntohs(udp.length) - UDP_HEADER;
            char *buffer = (char *) calloc(1, sizeof(amount));

            read = fread(&buffer, amount, 1, fp);
            if (read < 1)
            {
                fprintf(stderr, "Could not read Zerg Packet Header");
                fclose(fp);
                return NULL;
            }
            free(buffer);
            size -= amount;
            continue;

        }

        // Bytes remaining in file
        size -= sizeof(udp);

        // Read in Zerg Header
        read = fread(&zerg, ZERG_HEADER, 1, fp);
        if (read < 1)
        {
            fprintf(stderr, "Could not read Zerg Packet Header");
            fclose(fp);
            return NULL;
        }
        int length = ((ntohl(zerg.length)) >> 8 & 0XFFFFFF);

        // checking for length mismatch in zerg length to bytes total bytes
        // reminaing in file
        if (size < length)
        {
            fprintf(stderr,
                    "Packet length mismatch."
                    " Not enough bytes left in file\n");
            fclose(fp);
            return NULL;
        }


        int version = zerg.verType >> 4 & 0xF;
        int type = zerg.verType & 0xF;

        // Print statement to ensure proper data being read in
        //printf("Version   : %d\n", version);
        // Ensure proper zerg version number
        if (version != 1)
        {
            printf("Invalid Version Number\n");
            return NULL;

        }
        else
        {
            // Bytes remaining in file
            size -= length;
        }
        // Print statement to ensure proper data being read in
        //printf("To        : %d\n", ntohs(zerg.did));

        // Per Liam source is destination and destination is the source 
        // zerg ID
        int zergId = ntohs(zerg.did);

        // Zerg header length - 12 bytes of fixed length header equals
        // the Zerg payload size.
        int payloadSize = length - ZERG_HEADER;

        switch (type)
        {
        case 0:;
            // Payload is a message
            zergMessage(fp, payloadSize);
            break;
        case 1:
            // Payload is a Zerg Status
            zergInfo = zergStatus(fp, payloadSize, zergInfo);
            if (!zergInfo)
            {
                fprintf(stderr, "Invalid Zerg Status Information‽\n");
                fclose(fp);
                return NULL;
            }
            if (((double) zergInfo->hitPoints / (double) zergInfo->maxHp) *
                100 < (double) percentHealth)
            {
                int index = (*zergHealth)[0] + 1;

                (*zergHealth)[index] = zergId;
                (*zergHealth)[0] += 1;
                // If I have placed something in an index with a power of 2
                // then that is the end of the array, need to increase the 
                // size. Process ands the number against its 1s compliment
                // Test found at stackoverflow.com/questions/3638431/determine-
                // if-an-int-is-a-power-of-2-or-not-in-a-single-line
                if (((index + 1) & ~(index)) == (index + 1))
                {
                    *zergHealth =
                        realloc(*zergHealth, sizeof(int) * (index + 1) * 2);
                    if (!*zergHealth)
                    {
                        fprintf(stderr, "Unable to realloc memory‽\n");
                        return NULL;
                    }
                }
            }
            break;
        case 2:
            //Paylaod is a Command Instruction
            zergCommand(fp);
            break;
        case 3:
            //Payload is GPS Data
            gps = zergGPS(fp, payloadSize, gps);
            // Build the graph
            if (gps && abs(gps->altitude) < 11265.4)
            {
                graphMakeVertex(zergPos, zergId, gps->latitude, gps->longitude,
                                gps->altitude);
            }
            break;

        default:
            // Someone messed up, there is an unkown payload type‽
            printf("Uknown Payload Type‽\n");
            fclose(fp);
            return 0;
        }

        // Ensuring the program has read the entire packet, if 
        // there was padding bytes added to ensure minimum packet size
        int padding = pHead.lenDataCap - headerSize - length;

        if (padding != 0)
        {
            char *packetPadding = malloc(padding);

            printf("%d\n", padding);

            read = fread(packetPadding, padding, 1, fp);
            if (read < 1)
            {
                fprintf(stderr, "Could not read padding\n");
                return 0;
            }
            // Bytes remaining in file
            size -= padding;

            free(packetPadding);
            packetPadding = NULL;
        }

        // Malloc-ing memory for buff step taken from:
        // stackoverflow.com/questions/28645473/segmentation-fault-in-comparing-
        // the-second-byte-of-two-files
        char *buff = (char *) malloc(sizeof(char));

        if (!buff)
        {
            fprintf(stderr, "Unable to malloc space\n");
            fclose(fp);
            return 0;
        }
        // Reading in next byte to see if at the EOF or not.
        // Need to read, feof will not recognize EOF if you just
        // fseek to the next byte.
        read = fread(buff, 1, 1, fp);
        if (read < 1 && !feof(fp))
        {
            fprintf(stderr, "Could not read End of File Test\n");
            return 0;
        }

        // Only needed the malloc to advance the file pointer a byte,
        // the Valgrind overloard wants be to free the malloc.
        free(buff);
        buff = NULL;

        if (feof(fp))
        {
            break;
        }
        // Need to move the pointer back one so the next packet reads
        // in corectly.
        fseek(fp, -1, SEEK_CUR);

        // New line to seperate each indivudial Zerg Packet
        printf("\n");
    }

    // Closing the file upon coompletion
    fclose(fp);
    free(gps);
    free(zergInfo);
    return zergPos;

}
