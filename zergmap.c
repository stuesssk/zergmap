#define _XOPEN_SOURCE

 #include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>
#include <string.h>

#include "map.h"
#include "search.h"
#include "struct.h"
#include "zergdata.h"
#include "zergDecode.h"

int
main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s (-h ##)<file1> (<file2>...)\n", argv[0]);
        return EX_IOERR;
    }
    // Suppress getopt default error handling
    opterr = 0;

    // Lets get the option flags
    int c;
    size_t healthPercent = 10;
    int testing = 0;

    // Reading in the command line option (-h ## for percent health)
    while (-1 < (c = getopt(argc, argv, "h")))
    {
        char *errGetOpt;
        switch (c)
        {
        case 'h':
            ++testing;
            healthPercent = strtol(argv[optind], &errGetOpt, 10);
            if (*errGetOpt)
            {
                printf("Error: Number must follow -h\n");
                return EX_USAGE;
            }
            break;
        case '?':
            printf("Unknown option\n");
            printf("./ws [-h <number>] <file1> [<file2> ...]\n");
            return EX_USAGE;
        default:
            printf("Default option handler got '%c'\n", c);
            return EX_USAGE;
        }
    }

    int i = 1;
    // Skipping over any optind, if they were there
    if (testing)
    {
        i = optind + 1;
    }
    else
    {
        i = optind;
    }

    // And the graph was born
    graph *zergPos = createGraph();
    char *argument = NULL;

    // Will double the size when inserting into last spot of array
    // see zergDecode.c:296
    int *zergHealth = malloc(sizeof(int) * 2);
    // Using position zero to store length, so I don't have to pass
    // the number of elements along with the array.    
    zergHealth[0] = 0;

    // Feed me Seymour. Give me the data
    while (i < argc)
    {
        argument = argv[i];
        decodeFunc(argument, healthPercent, zergPos, &zergHealth);
        ++i;
    }

    // Need to know how many zerg are on the graph
    size_t totalZerg = nodeCount(zergPos);

    // Printing out Zerg with low Health
    printf("Low Health Zerg (<%zd):\n", healthPercent);
    for (int i = 1; i <= zergHealth[0]; ++i)
    {
        printf("#%d\n", zergHealth[i]);
    }
    free(zergHealth);

    // Ensuring we don't have to kil more than half before telling who
    // to eliminate. Storing in array all zerg we want to kill.
    int *zergToKill = calloc((totalZerg / 2 + 2), sizeof(zergToKill));

    zergToKill[0] = 0;

    // Eliminate zerg that have been flaged as being to close (< 1.25yds)
    struct node *zerg = zergPos->vertices;
    while (zerg != NULL)
    {
        if (zerg->tooClose)
        {
            zergToKill[zergToKill[0] + 1] = zerg->zergId;
            ++zergToKill[0];
        }
        zerg = zerg->next;
    }

    if (!totalZerg)
    {
        // If there are no nodes on the graph, time to end the program
        // nothing else to see.
        printf("\nNo Zerg GPS data was recieved‽\n");
        goto cleanup;
    }

    // Looking for nodes with 1 or 0 adjacencies. These can 
    // be removed instantly. Can't have two paths with only one
    // adjacency
    int zergNum = 0;

    do
    {

        if (zergToKill[0] > (int) (totalZerg / 2))
        {
            printf
                ("\nToo many changes required to achieve Pyschic Cascade.\n");
            goto cleanup;
        }
        // Function will return zerg number if any node has less than 
        // two adjacencies, or returns 0 if all nodes have atleast 2
        zergNum = vertexEdgeCount(zergPos, false);
        if (zergNum)
        {
            // Remove the zerg from the graph, but just store the zerg
            // number for now
            zergToKill[zergToKill[0] + 1] = zergNum;
            ++zergToKill[0];
            bool deleted = deleteZerg(zergPos, zergNum);

            if (!deleted)
            {
                printf("Unable to eliminate zerg %d\n", zergNum);
                goto cleanup;
            }
        }
        size_t nodes = nodeCount(zergPos);

        if (nodes == 2 && zergToKill[0] < (int) totalZerg / 2)
        {
            // If max zerg kills are not reached and 2 zerg left
            // map is complete
            break;
        }
    } while (zergNum);

    // Modifying weights of internal connects to have the search func
    // prefer outside paths. Internal paths are ones where a node has more
    // than 2 adjacencies.
    zerg = zergPos->vertices;
    // Updating the number of nodes each node is adjacent to
    edgeCount(zergPos);
    while (zerg)
    {
        struct edge *adj = zerg->edges;
        while (adj)
        {
            if (zerg->numEdges > 2 && adj->to->numEdges > 2)
            {
                adj->dist = INF;
            }
            adj = adj->next;
        }
        zerg = zerg->next;
    }



    // Getting the vertex with the least number of adjacencies
    zergNum = vertexEdgeCount(zergPos, true);

    // Looking to see if graph contains a choke point, stoping 
    // nodes from have two completely unique paths to other nodes
    // and trimming the graph to not have a choke point
    int elimZerg = search(zergPos, zergNum);
    while (elimZerg)
    {
        if (zergToKill[0] > (int) (totalZerg / 2))
        {
            printf
                ("\nToo many changes required to achieve Pyschic Cascade.\n");
            goto cleanup;
        }
        zergToKill[zergToKill[0] + 1] = zergNum;
        ++zergToKill[0];
        bool deleted = deleteZerg(zergPos, zergNum);

        if (!deleted)
        {
            printf("Unable to eliminate zerg %d\n", zergNum);
            goto cleanup;
        }
        elimZerg = search(zergPos, zergNum);
    }


    // All the program and here is the ouput you want. Printing
    // zerg needed to eliminate to cause a pyschic cascade.
    if (zergToKill[0] == 0)
    {
        printf("\nALL ZERG ARE IN POSITION\n");
    }
    else
    {
        printf("\nNetwork ALterations:\n");
        for (int i = 1; i <= zergToKill[0]; ++i)
        {
            printf("#%d\n", zergToKill[i++]);
        }
    }
    // Valgrind overlords require a scrafice of all the mallocs before 
    // exiting the program.
cleanup:
    destroyGraph(zergPos);
    free(zergToKill);
    return 0;

}
