#ifndef STRUCT_H
#define STRUCT_H

#include <stdio.h>
#include <stdint.h>

typedef struct zergData
{
    int hitPoints;
    unsigned int maxHp;
}zergData;

typedef struct gpsData
{
    double latitude;
    double longitude;
    double altitude;
}position;

// @breif Stores PCAP File Header data for analysis
struct __attribute__((packed)) capHeader
{
    uint32_t fileID:32;
    uint32_t majorVer:16;
    uint32_t minorVer:16;
    uint32_t GMTOffset:32;
    uint32_t delta:32;
    uint32_t maxLength:32;
    uint32_t layerType:32;
};

// Stores PCAP Packet Header data for analysis
struct packetHeader
{
    uint32_t unixEpoch:32;
    uint32_t usEpoch:32;
    uint32_t lenDataCap:32;
    uint32_t untruncatedLen:32;
};

// Stores Ethernet Frame data for analysis
struct __attribute__((packed)) ethFrame
{
    uint64_t dmac:48;
    uint64_t smac:48;
    uint32_t ethType:16;
};

// Stores IPv4 Header data for analysis
struct __attribute__((packed)) ipv4Header
{

    uint32_t versionIhl:8;
    uint32_t dscpEcn:8;
    uint32_t length:16;
    uint32_t id:16;
    uint32_t flagsFrag:16;
    uint32_t ttl:8;
    uint32_t protocol:8;
    uint32_t checksum:16;
    uint32_t sIP:32;
    uint32_t dIP:32;
};

// Stores UDP Header data for analysis
struct __attribute__((packed)) udpHeader
{
    uint32_t sPort:16;
    uint32_t dPort:16;
    uint32_t length:16;
    uint32_t checksum:16;
};

// Stores Zerg Header data for analysis
struct __attribute__((packed)) zergHeader
{
    //Zerg Header
    uint32_t verType:8;
    uint32_t length:24;
    uint32_t sid:16;
    uint32_t did:16;
    uint32_t seqID:32;
};

// Stores Zerg GPS Payload data for analysis
struct __attribute__((packed)) gpspayload
{
    int64_t longitude;
    int64_t latitude;
    int32_t altitude;
    int32_t bearing;
    int32_t speed;
    int32_t accuracy;
};

// Stores Zerg Status Payload data for analysis
struct __attribute__((packed)) statuspayload
{
    uint32_t hp:24;
    uint32_t armor:8;
    uint32_t maxHp:24;
    uint32_t breed:8;

    // need to be able to account for negative values sent over wire
    // since speed is always a positive value but stored as a binary 32.
    // Physics definition: speed == |velocity|
    int32_t speed;
};

// Stores Zerg Command Payload data for analysis
struct __attribute__((packed)) commandpayload
{
    uint32_t command:16;
    int32_t param1:16;
    int32_t param2;
};

// Stores IPv6 Header data for analysis
struct __attribute__((packed)) ipv6Header
{
    uint32_t verClassFlow;
    uint32_t payloadLen:16;
    uint32_t nextHeader:8;
    uint32_t hopLimit:8;
    uint64_t source1;
    uint64_t source2;
    uint64_t dest1;
    uint64_t dest2;
};

// Max number of characters in zergh breed is 9, 16 give a bit of buffer
enum
{ MAX_ZERG_BREED = 16 };

// Size of Zerg Header
enum
{ZERG_HEADER = 12 };

union ipHeader
{
    struct ipv4Header ipv4;
    struct ipv6Header ipv6;
} ih;
#endif
