#ifndef ZERGDATA_H
#define ZERGDATA_H

#include <stdio.h>

#include "struct.h"

uint64_t
ntonll(uint64_t value);

zergData *
zergStatus(FILE * fp, int length, zergData *newStatus);

void
zergCommand(FILE * fp);

position *
zergGPS(FILE * fp, int payloadSize, position *newGps);

void
zergMessage(FILE * fp, int payloadSize);

#endif
